# BirdConnect
LabVIEW Code for "A System for Controlling Vocal Communication Networks". 

Software Required: LabVIEW 2018 32 bit, Realtime module, FPGA-module, Digital Filter design Toolkit, CompactRIO Driver 18.0. 
Hardware Required: CompactRIO Controller 9063 or 9076 with modules NI-9215 (4xAI 16 bit) and NI-9263 (4xAO 16 bit). 

This code is licensed via the MIT License (see included file LICENSE).
Copyright: (c) 2020 ETH Zurich, Jörg Rychen, Richard H.R. Hahnloser

Reference (please cite):
A System for Controlling Vocal Communication Networks 
Jörg Rychen, Diana I. Rodrigues, Tomas Tomka, Linus Rüttimann, Homare Yamahachi, Richard H. R. Hahnloser
DOI: ...
